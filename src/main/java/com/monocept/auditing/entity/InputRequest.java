package com.monocept.auditing.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;


@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class InputRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Request request;

	

	public Request getRequest() {
		return request;
	}



	public void setRequest(Request request) {
		this.request = request;
	}



	@Override
	public String toString() {
		return "InputRequest [request=" + request + "]";
	}
	
	

}
