package com.monocept.auditing.entity;

public class OutputResponse {
	
	private Response response;
	
	
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "OutputResponse [response=" + response + "]";
	}
	
	
}
