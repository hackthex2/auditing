package com.monocept.auditing.entity;


public class TaskDetails {
	
	private String name;
    private String priority;
    private String status;
    private String taskId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaskDetails [name=" + name + ", priority=" + priority + ", status=" + status + ", taskId=" + taskId
				+ "]";
	}
    
    
}
