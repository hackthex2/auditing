package com.monocept.auditing.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.monocept.auditing.entity.AuditDetails;

@Repository
public interface AuditingRepository extends MongoRepository<AuditDetails, String> {
	
}
