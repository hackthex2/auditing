package com.monocept.auditing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.monocept.auditing.entity.AuditDetails;
import com.monocept.auditing.entity.InputRequest;
import com.monocept.auditing.entity.OutputResponse;
import com.monocept.auditing.service.AuditingService;

@RestController
@RequestMapping(value = "/fulfillment/api/audit")
public class AuditController {
	
	@Autowired
	private AuditingService auditService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<Object> saveAuditDetails(@RequestBody InputRequest inputRequest) {
	
		AuditDetails auditDetails=inputRequest.getRequest().getAuditDetails();
		System.out.println(auditDetails);
		OutputResponse response = auditService.saveauditData(auditDetails);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}

}
