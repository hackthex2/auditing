package com.monocept.auditing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monocept.auditing.entity.AuditDetails;
import com.monocept.auditing.entity.OutputResponse;
import com.monocept.auditing.entity.Response;
import com.monocept.auditing.repository.AuditingRepository;
import com.monocept.auditing.service.AuditingService;

@Service
public class AuditingServiceImpl implements AuditingService{
	
	@Autowired
	AuditingRepository auditingRepository;

	@Override
	public OutputResponse saveauditData(AuditDetails auditDetails) {
		OutputResponse outputResponse = new OutputResponse();
		Response response = new Response();
		System.out.println(auditDetails);
		AuditDetails msg = auditingRepository.save(auditDetails);
		String id =msg.getId();
		if(id != null) {
			response.setStatus(true);
			response.setMessage("save Successfully");
		}else {
			response.setStatus(false);
			response.setMessage("Not save Successfully");
		}
		outputResponse.setResponse(response);
		return outputResponse;
	}

}
