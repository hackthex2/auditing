package com.monocept.auditing.service;

import com.monocept.auditing.entity.AuditDetails;
import com.monocept.auditing.entity.OutputResponse;

public interface AuditingService {
	
	public OutputResponse saveauditData(AuditDetails auditDetails);

}
